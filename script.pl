#!/usr/bin/perl  
use warnings;
use strict;
use HTML::Template;
use DBI;
use DBD::mysql;

# 1. Set up database coonection (replace with your database details)
my $driver = "mysql";  
my $database = "perl_task";  
my $dsn      = "dbi:$driver:database=$database";  
my $user     = "e_card_admin";  
my $password = "e_card_password";  
my $dbh = DBI->connect($dsn, $user, $password, 
{ RaiseError => 1}
);  

# 2. Extract the data
my $sth = $dbh->prepare('
   select recno, first_name, last_name, email, phone_number
   from users
');
$sth->execute();

# 3. Make proper data structure for HTML::Template
my $rows;
push @{$rows}, $_ while $_ = $sth->fetchrow_hashref();


# 4. Instantiate the template and substitute the values
my $template = HTML::Template->new(filename => 'users_list.html');
$template->param(USERS_LOOP => $rows);

# 5. Disconnect
$dbh->disconnect();

# 6. Handle file
my $filename = 'users_list_ready.html';  
open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";  
print $fh $template->output();  
close $fh;  
print "done\n";  
